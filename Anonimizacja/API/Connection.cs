﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Leaf.xNet;


namespace Anonimizacja.API
{
    class Connection
    {
        public string auth, response;
        private string _baseURL = "https://anonimizacja.weblider24.pl/api";
        HttpRequest request = new HttpRequest();


        private void updateRequest()
        {
            this.request = new HttpRequest();
            this.request.AddHeader("Authorization", "Basic " + this.auth);
            this.request.AddHeader("Accept", "application/json");
            this.request.AddHeader("content-type", "application/json");
            this.request.IgnoreProtocolErrors = true;
        }

        public string doPost(string url, RequestParams reqParam)
        {
            updateRequest();
            return request.Post(this._baseURL + url, reqParam).ToString();
        }

        public string doGet(string url)
        {
            updateRequest();
            return request.Get(this._baseURL + url).ToString();
        }
    }
}
