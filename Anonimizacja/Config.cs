﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Anonimizacja
{
    static class Config
    {
        public static string CITIES_FILE = "cities.txt";
        public static string DICTIONARY_FILE = "dictionary.txt";
        public static bool isInitialized = true;
        public static String fileName { get; set; }
        public static String filePath { get; set; }
        public static String directory { get; set; }
        public static String WATERMARK { get; set; }
    }
}
