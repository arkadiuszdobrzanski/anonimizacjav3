﻿using iText.Kernel.Pdf;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Anonimizacja.Layout
{
    /// <summary>
    /// Logika interakcji dla klasy Home.xaml
    /// </summary>
    public partial class Home : UserControl
    {
        public Modules.Update checkUpdate;
        public Modules.Cities citiesArray;
        private Modules.MainApp mainApp;
        private Modules.MultiPDFChoosen multiPDFChoosen;
        private Modules.Licences licences;
        public List<string> Arrayka = new List<string> { "" };
        private bool isImage = false;
        private bool isDefault2 = false;
        private List<string> choosenFilesPDFs;
        public Home()
        {
            this.checkUpdate = new Modules.Update();
            InitializeComponent();
            this.licences = new Modules.Licences();
            this.multiPDFChoosen = new Modules.MultiPDFChoosen();
            this.mainApp = new Modules.MainApp();
            this.citiesArray = new Modules.Cities();
        }

        private void execute_Click(object sender, RoutedEventArgs e)
        {

            if (this.hideDic.IsChecked == true)
            {
                Modules.Dictionary dictionaryService = new Modules.Dictionary();
                foreach (var foo in dictionaryService.loadArray())
                {
                    this.Arrayka.Add(foo.ToString().TrimEnd(' '));
                }
            }

            if (!string.IsNullOrWhiteSpace(this.searchValue.Text))
            {
                this.Arrayka.Add(this.searchValue.Text);
            }

            if (this.hidePesel.IsChecked == true)
            {

                this.Arrayka.Add("[0-9]{11}");
            }

            if (this.hidePostCode.IsChecked == true)
            {
                this.Arrayka.Add("[0-9]{2}-[0-9]{3}");
            }
            if(this.isDefault.IsChecked == true)
            {
                this.isDefault2 = true;
            }
           

            if (this.hideCity.IsChecked == true)
            {
                try
                {
                    foreach (var foo in this.citiesArray.getCitites())
                    {
                        this.Arrayka.Add(foo.ToString().TrimEnd(' '));
                    }
                }
                catch(Exception)
                {
                    System.Windows.MessageBox.Show("Nie znaleziono pliku z miastami.");
                }
               
            }

            //wywołanie anonimizacji
            if (this.multiPDF.IsEnabled)
            {
                this.mainApp.executeAllFiles(this.Arrayka, this.isImage, this.isDefault2, true, choosenFilesPDFs);
            }
            else
            {
                this.mainApp.execute(this.Arrayka, this.isImage, this.isDefault2, false, null);

            }


        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (PdfView.searchValues(this.searchValue.Text))
            {
                if (!this.tags.Items.Contains(this.searchValue.Text))
                {
                    this.tags.Items.Add(this.searchValue.Text);
                    //this.tags.Background = new SolidColorBrush(Color.FromArgb(255, 235, 235, 235));
                }
            }
        }
        private void Reset_click(object sender, RoutedEventArgs e)
        {
            this.tags.Items.Clear();
            PdfView.ClearAll();
        }
        private void settingsCLick(object sender, RoutedEventArgs e)
        {
           
        }
        private void MultiPDF(object sender, RoutedEventArgs e)
        {
            this.choosenFilesPDFs = this.multiPDFChoosen.getPDFs();
            if (this.choosenFilesPDFs != null)
            {
                this.multiPDF.IsEnabled = true;
            }
           
            
        }

        private void editDictionary(object sender, RoutedEventArgs e)
        {
            Windows.DictionaryMainWindow dicWindow = new Windows.DictionaryMainWindow();
            dicWindow.Show();
        }

        private void chooseFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Pliki zdjęć (*.png)|*.png|Wszystkie pliki (*.*)|*.*";
            openFileDialog.Multiselect = false;
            if (openFileDialog.ShowDialog() == true)
            {
                Config.WATERMARK = openFileDialog.FileName;
            }
            
            this.chooseFile.BorderBrush = Brushes.Red;
            //this.chooseXY.BorderBrush = Brushes.Transparent;
           
        }
        
       
        private void chooseClassic_Click(object sender, RoutedEventArgs e)
        {
            
            //this.chooseXY.BorderBrush = Brushes.Transparent;
            this.classic.BorderBrush = Brushes.Red;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var dict = new Dictionary<string, string>();
            PdfDocument pdf = new PdfDocument(new PdfReader(@"" + Config.filePath));
            PdfDictionary infoDictionary = pdf.GetTrailer().GetAsDictionary(PdfName.Info);
            foreach (PdfName key in infoDictionary.KeySet())
            {
                dict[key.ToString()] = infoDictionary.GetAsString(key).ToString();
                //Console.WriteLine($"{key}: {infoDictionary.GetAsString(key)}");
            }

            foreach(var item in dict)
            {
                System.Windows.Forms.MessageBox.Show(item.Key + " : " + item.Value);

            }


        }
    }
    
}
