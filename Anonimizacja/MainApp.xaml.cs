﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Anonimizacja
{
    /// <summary>
    /// Logika interakcji dla klasy MainApp.xaml
    /// </summary>
    public partial class MainApp : Window
    {
        public Modules.Update checkUpdate;
        public Modules.Cities citiesArray;
        private Modules.MainApp mainApp;
        private Modules.MultiPDFChoosen multiPDFChoosen;
        private Modules.Licences licences;
        private Windows.OCR ocr; 
        public List<string> Arrayka = new List<string> { "" };
        private bool isImage = false;


        public MainApp()
        {
            InitializeComponent();
        }
        private void closeWindow(object sender, RoutedEventArgs e)
        {
            System.Environment.Exit(1);

        }

        private void minWindow(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }
        private void runOCR(object sender, RoutedEventArgs e)
        {
            this.ocr = new Windows.OCR();
            this.ocr.Show();
        }

        private void openFile(object sender, RoutedEventArgs e)
        {
            this.Hide();
            Config.isInitialized = false;
            MainApp mainApp = new MainApp();
            mainApp.Show();
        }

        private void openIMGToPDFBox(object sender, RoutedEventArgs e)
        {
            Windows.MultiPDF multiPDF = new Windows.MultiPDF();
            multiPDF.Show();
        } 
        private void yourProjects(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("test");
        }
    }
}
