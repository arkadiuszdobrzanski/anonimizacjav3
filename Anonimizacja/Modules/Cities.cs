﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.IO;

namespace Anonimizacja.Modules
{
    public class Cities
    {
        public string sPath = Config.CITIES_FILE;
        private string _line;
        private readonly string[]  _arrayCities = new string[953];

        public Cities()
        {
            
        }


        public string[] getCitites()
        {
            if (File.Exists(this.sPath))
            {
                var file = new StreamReader(this.sPath);
                int i = 0;
                while ((this._line = file.ReadLine()) != null)
                {
                    this._arrayCities[i] = this._line;
                    i++;
                }
            }
            return this._arrayCities;
        }   
    }
}
