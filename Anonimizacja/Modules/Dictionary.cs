﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Linq;

namespace Anonimizacja.Modules
{
    public class Dictionary
    {
        private string _sPath = Config.DICTIONARY_FILE;
        private string _line;
        public List<string> dictionary = new List<string> { "" };
        private ListBox _dictionaryList;

        public Dictionary(ListBox dictionaryList)
        {
            this._dictionaryList = dictionaryList;
        }

        public Dictionary()
        {
          
        }

        public string[] loadItemsFromFile()
        {
            if (File.Exists(this._sPath))
            {
                string line;
                var file = new StreamReader(this._sPath);
                while ((line = file.ReadLine()) != null)
                {
                    this._dictionaryList.Items.Add(line);
                }
            }
            return this._dictionaryList.Items.OfType<string>().ToArray(); ;
        }

        public List<string> loadArray()
        {
            if (File.Exists(this._sPath))
            {
                var file = new StreamReader(this._sPath);
                int i = 0;
                while ((this._line = file.ReadLine()) != null)
                {
                    this.dictionary.Add(this._line);
                    //this.dictionary[i] = this._line;
                    //i++;
                }
            }
            return this.dictionary;
        }


        public bool saveDictionary(ListBox dictionary)
        {
            this._dictionaryList = dictionary;
            StreamWriter SaveFile = new StreamWriter(this._sPath);
            foreach (var item in this._dictionaryList.Items)
            {
                SaveFile.WriteLine(item.ToString());
            }
            SaveFile.Close();
            return true;

        }

        public void addItem(string inputValue)
        {
            this._dictionaryList.Items.Add(inputValue);
        }

        public void removeItem()
        {
            this._dictionaryList.Items.RemoveAt(this._dictionaryList.Items.IndexOf(this._dictionaryList.SelectedItem));
        }

    }
}
