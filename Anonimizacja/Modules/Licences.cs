﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Anonimizacja.Modules
{
    class Licences
    {
        private API.Connection _auth = new API.Connection();
        public bool checkLicence(string licenceKey)
        {
            return this._auth.doGet("/checkLicence/" + licenceKey).Contains("id");
        }
    }
}
