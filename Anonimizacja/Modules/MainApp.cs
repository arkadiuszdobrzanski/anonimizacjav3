﻿
using iText.Kernel.Pdf;
using iText.PdfCleanup.Autosweep;
using System.Text.RegularExpressions;
using iText.Kernel.Colors;
using iText.Kernel.Geom;
using System.Windows.Forms;
using iText.Kernel.Pdf.Canvas;
using iText.Layout;
using iText.Layout.Element;
using iText.Kernel.Pdf.Colorspace;
using iText.IO.Image;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Anonimizacja.Modules
{
    class MainApp
    {
        private string _imageFilePath;

        private string _imagePosition;


        public Rectangle _pageSize;

        private string customText;

        private bool isCustomText = false;
        public DateTime localDate = DateTime.Now;
        public PDF pdfModule = new PDF();

        public MainApp()
        {

        }

      
        public void execute(List<string> Arrayka, bool isImage, bool isDefault, bool IsMultiPDFChoosen, List<string> choosenFilesPDFs)
        {
           
            PdfDocument pdf = new PdfDocument(new PdfReader(@"" + Config.filePath), new PdfWriter(@"" + Config.filePath + "_filled.pdf"));
            Document document = new Document(pdf);
        
            foreach (var element in Arrayka)
            {
                ICleanupStrategy cleanupStrategy1 = new RegexBasedCleanupStrategy(new Regex(@"" + element + "", RegexOptions.IgnoreCase)).SetRedactionColor(ColorConstants.BLACK);
                PdfAutoSweep autoSweep = new PdfAutoSweep(cleanupStrategy1);
                autoSweep.CleanUp(pdf);
            }

            if (this.isCustomText)
            {
                this.pdfModule.insertText(pdf, this.customText);
            }
            if (isDefault)
            {
                int numberOfPages = pdf.GetNumberOfPages();
                string text = "Anonimizowano dnia "+ this.localDate.Day + "/" + this.localDate.Month + "/" + this.localDate.Year + " przez XYZ o godzinie "+this.localDate.Hour + ":" + this.localDate.Minute;

                for (int i = 1; i <= numberOfPages; i++)
                {
                    PdfPage page = pdf.GetPage(i);
                    PdfCanvas pdfCanvas = new PdfCanvas(page);
                    Rectangle rectangle = new Rectangle(25, -50, page.GetPageSizeWithRotation().GetWidth(), 100);
                    pdfCanvas.Rectangle(rectangle);

                    Canvas canvas = new Canvas(pdfCanvas, pdf, rectangle);

                    Text customTextValue = new Text(text).SetFontColor(ColorConstants.LIGHT_GRAY);
                    Paragraph p = new Paragraph().Add(customTextValue);
                    canvas.Add(p);
                    canvas.Close();
                }
            }
            if (Config.WATERMARK != null)
            {
                this.pdfModule.insertImage(pdf, Config.WATERMARK, _imagePosition, _pageSize);

            }
            pdf.Close();
            MessageBox.Show("Success");
        }

        public void executeAllFiles(List<string> Arrayka, bool isImage, bool isDefault, bool IsMultiPDFChoosen, List<string> choosenFilesPDFs)
        {
            foreach(var item in choosenFilesPDFs)
            {
                Config.filePath = item;
                PdfDocument pdf = new PdfDocument(new PdfReader(@"" + item), new PdfWriter(@"" + item + "_filled.pdf"));
                Document document = new Document(pdf);

                foreach (var element in Arrayka)
                {
                    ICleanupStrategy cleanupStrategy1 = new RegexBasedCleanupStrategy(new Regex(@"" + element + "", RegexOptions.IgnoreCase)).SetRedactionColor(ColorConstants.BLACK);
                    PdfAutoSweep autoSweep = new PdfAutoSweep(cleanupStrategy1);
                    autoSweep.CleanUp(pdf);
                }

                if (this.isCustomText)
                {
                    this.pdfModule.insertText(pdf, this.customText);
                }
                if (isDefault)
                {
                    int numberOfPages = pdf.GetNumberOfPages();
                    string text = "Anonimizowano dnia " + this.localDate.Day + "/" + this.localDate.Month + "/" + this.localDate.Year + " przez XYZ o godzinie " + this.localDate.Hour + ":" + this.localDate.Minute;

                    for (int i = 1; i <= numberOfPages; i++)
                    {
                        PdfPage page = pdf.GetPage(i);
                        PdfCanvas pdfCanvas = new PdfCanvas(page);
                        Rectangle rectangle = new Rectangle(25, -50, page.GetPageSizeWithRotation().GetWidth(), 100);
                        pdfCanvas.Rectangle(rectangle);

                        Canvas canvas = new Canvas(pdfCanvas, pdf, rectangle);

                        Text customTextValue = new Text(text).SetFontColor(ColorConstants.LIGHT_GRAY);
                        Paragraph p = new Paragraph().Add(customTextValue);
                        canvas.Add(p);
                        canvas.Close();
                    }
                }
                if (Config.WATERMARK != null)
                {
                    this.pdfModule.insertImage(pdf, Config.WATERMARK, _imagePosition, _pageSize);

                }
                pdf.Close();
            }
            
            MessageBox.Show("Success");
        }

        public string openFile()
        {
            OpenFileDialog choofdlog = new OpenFileDialog();
            choofdlog.Filter = "Plik obrazu (*.jpg)|*.jpg|Plik obrazu (*.png)|*.png";
            choofdlog.FilterIndex = 1;
            choofdlog.Multiselect = false;

            if (choofdlog.ShowDialog() == DialogResult.OK)
            {
                this._imageFilePath = choofdlog.FileName;
            }
            return this._imageFilePath;
        }

        public void setImagePosition(string imagePosition)
        {
            this._imagePosition = imagePosition;
        }

        public void addCustomText(string foo)
        {
            this.customText = foo;
            this.isCustomText = true;
        }

    }
}
