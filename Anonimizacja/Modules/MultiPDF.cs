﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using Microsoft.Win32;
using Tesseract;

namespace Anonimizacja.Modules
{
    //TUTAJ ZDJĘCIA, WYPAD STĄD
    class MultiPDF
    {
        private List<string> _imagesPaths = new List<string> {  };

        public void setImagesPaths()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Pliki zdjęć (*.png;*.jpeg)|*.png;*.jpeg|All files (*.*)|*.*";
            openFileDialog.Multiselect = true;
            if (openFileDialog.ShowDialog() == true)
            {
                foreach (String file in openFileDialog.FileNames)
                {
                    this._imagesPaths.Add(file);

                }
            }
            if ((_imagesPaths != null))
            {
                MessageBox.Show("Zdjęcia dodane. Przetwórz je na PDF");
            }

        }
        public string[] getImagesPaths()
        {
            return this._imagesPaths.ToArray();
        }

        public void convertImagesToPDF(string fileName)
        {
            var folderPath = Directory.CreateDirectory(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Anonimizacja"));

            using (IResultRenderer renderer = Tesseract.PdfResultRenderer.CreatePdfRenderer(folderPath.FullName +"/"+ fileName, @".\tessdata", false))
            {
                using (renderer.BeginDocument(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Anonimizacja") + "/" + fileName + ".pdf"))
                {
                    string configurationFilePath = @".\tessdata";
                    string configfile = Path.Combine(@".\tessdata", "pdf");
                    using (TesseractEngine engine = new TesseractEngine(configurationFilePath, "pol", EngineMode.TesseractAndLstm, configfile))
                    {
                        foreach(dynamic arrayList in _imagesPaths)
                        {
                            using (Pix img = Pix.LoadFromFile(arrayList.ToString()))
                            {
                                using (var page = engine.Process(img, Path.GetFileNameWithoutExtension(arrayList.ToString())))
                                {
                                    renderer.AddPage(page);
                                }
                            }
                        }
                        
                    }
                }
            }
            MessageBox.Show("Success. PDF utworzony w folderze: " + folderPath.FullName);
        }
    }
}
