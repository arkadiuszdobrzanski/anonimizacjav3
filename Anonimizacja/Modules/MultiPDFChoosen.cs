﻿using iText.Kernel.Colors;
using iText.Kernel.Pdf;
using iText.PdfCleanup.Autosweep;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;

namespace Anonimizacja.Modules
{
    class MultiPDFChoosen
    {
        private List<string> _pdfsPath = new List<string> { };
        public List<string> getPDFs()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Pliki PDF (*.pdf)|*.pdf|Wszystkie pliki (*.*)|*.*";
            openFileDialog.Multiselect = true;
            if (openFileDialog.ShowDialog() == true)
            {
                foreach (String file in openFileDialog.FileNames)
                {
                    this._pdfsPath.Add(file);

                }
            }
            if (this._pdfsPath != null || this._pdfsPath.Count != 0  )
            {
                MessageBox.Show("Wykryto "+ this._pdfsPath.Count + " plików PDF.");
                return _pdfsPath;
            }
            else
            {
                MessageBox.Show("Błąd .");
                return null;
            }
            

        }

        public void execute(List<string> Arrayka)
        {
            foreach(dynamic foo in _pdfsPath)
            {
                PdfDocument pdf = new PdfDocument(new PdfReader(@"" + foo), new PdfWriter(@"" + foo + "_filled.pdf"));
                foreach (var element in Arrayka)
                {
                    ICleanupStrategy cleanupStrategy1 = new RegexBasedCleanupStrategy(new Regex(@"" + element + "", RegexOptions.IgnoreCase)).SetRedactionColor(ColorConstants.BLACK);
                    PdfAutoSweep autoSweep = new PdfAutoSweep(cleanupStrategy1);
                    autoSweep.CleanUp(pdf);
                }
                pdf.Close();
            }
           
        }
    }
}
