﻿using System.Collections.Generic;
using System.IO;
using System.Collections;
using Tesseract;
using System;
using System.Diagnostics;

namespace Anonimizacja.Modules
{
    class OCR
    {
        public void OCRtoPDF(string filePath)
        {
            using (IResultRenderer renderer = Tesseract.PdfResultRenderer.CreatePdfRenderer(@"" + Path.GetDirectoryName(filePath) + "/" + Path.GetFileNameWithoutExtension(filePath), @".\tessdata\", false))
            {
                using (renderer.BeginDocument(Path.GetFileNameWithoutExtension(filePath)))
                { 
                    string configurationFilePath = @".\tessdata";
                    string configfile = Path.Combine(@".\tessdata", "pdf");
                    using (TesseractEngine engine = new TesseractEngine(configurationFilePath, "pol", EngineMode.TesseractAndLstm, configfile))
                    {
                        using (Pix img = Pix.LoadFromFile(filePath))
                        {
                            using (var page = engine.Process(img, Path.GetFileNameWithoutExtension(filePath)))
                            {
                                renderer.AddPage(page);
                            }
                        }
                    }
                }
            }
        }
    }
}
