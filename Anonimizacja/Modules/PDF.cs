﻿using System;
using System.Collections.Generic;
using System.Text;
using iText.Kernel.Pdf;
using iText.PdfCleanup.Autosweep;
using System.Text.RegularExpressions;
using iText.Kernel.Colors;
using iText.Layout;
using iText.IO.Image;
using iText.Layout.Element;
using iText.Kernel.Geom;
using System.Windows.Forms;
using iText.Kernel.Pdf.Canvas;

namespace Anonimizacja.Modules
{
    class PDF
    {
        public System.Drawing.Bitmap ChangeOpacity(System.Drawing.Image img, float opacityvalue)
        {
            System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(img.Width, img.Height); // Determining Width and Height of Source Image
            System.Drawing.Graphics graphics = System.Drawing.Graphics.FromImage(bmp);
            System.Drawing.Imaging.ColorMatrix colormatrix = new System.Drawing.Imaging.ColorMatrix();
            colormatrix.Matrix33 = opacityvalue;
            System.Drawing.Imaging.ImageAttributes imgAttribute = new System.Drawing.Imaging.ImageAttributes();
            imgAttribute.SetColorMatrix(colormatrix, System.Drawing.Imaging.ColorMatrixFlag.Default, System.Drawing.Imaging.ColorAdjustType.Bitmap);
            graphics.DrawImage(img, new System.Drawing.Rectangle(0, 0, bmp.Width, bmp.Height), 0, 0, img.Width, img.Height, System.Drawing.GraphicsUnit.Pixel, imgAttribute);
            graphics.Dispose();   // Releasing all resource used by graphics 
            return bmp;
        }
        public void insertImage(PdfDocument pdf, string imageFilePath, string imagePosition, Rectangle _pageSize)
        {
            var bmp = this.ChangeOpacity(System.Drawing.Image.FromFile(imageFilePath), 0.6f);
            int numberOfPages = pdf.GetNumberOfPages();
            bmp.Save(System.IO.Path.GetTempPath() + "/asd.jpg");
            //debug
            string imageFilePath2 = System.IO.Path.GetTempPath() + "/asd.jpg";
            //
            Document document = new Document(pdf);
            ImageData imageData = ImageDataFactory.Create(imageFilePath2);

            for (int i = 1; i <= numberOfPages; i++)
            {
                _pageSize = pdf.GetPage(i).GetPageSize();
                this.imageAddHelper(numberOfPages, imageData, _pageSize.GetWidth() - (_pageSize.GetWidth() / 2) - 150, _pageSize.GetHeight() - (_pageSize.GetHeight() / 2) - 150, document, pdf, i);

            }

        }

        public void imageAddHelper(int numberOfPages, ImageData imageData, float x, float y, Document document, PdfDocument pdf, int i)
        {
            Image image = new Image(imageData).ScaleAbsolute(300, 300).SetFixedPosition(i, x, y);
            document.Add(image);

        }

        public void insertText(PdfDocument pdf, string customText)
        {

            int numberOfPages = pdf.GetNumberOfPages();
            for (int i = 1; i <= numberOfPages; i++)
            {
                PdfPage page = pdf.GetPage(i);
                PdfCanvas pdfCanvas = new PdfCanvas(page);
                Rectangle rectangle = new Rectangle(25, 25, 100, 100);
                pdfCanvas.Rectangle(rectangle);

                Canvas canvas = new Canvas(pdfCanvas, pdf, rectangle);

                Text customTextValue = new Text(customText).SetFontColor(ColorConstants.BLACK);
                Paragraph p = new Paragraph().Add(customTextValue);
                canvas.Add(p);
                canvas.Close();
            }
        }
    }
}
