﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using System.Windows;

namespace Anonimizacja.Modules
{
    public class Update
    {
        Assembly assembly = Assembly.GetEntryAssembly();
        private string _line;
        public void updateCheck()
        {
            //sprawdzanie czy w pliku na serwerze jest inna wersja jeśli tak, uruchom updater
            string fileNameGet = "anonimizacja.txt";

            FtpWebRequest reqFTP;

            reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://"
                       + "anonimizacja.weblider24.pl" + "/web/public/desktop-app/" + fileNameGet));
            reqFTP.Method = WebRequestMethods.Ftp.DownloadFile;
            reqFTP.UseBinary = false;
            reqFTP.Credentials = new NetworkCredential("weblider_seba", "123123123");

            FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
            Stream ftpStream = response.GetResponseStream();

            var file = new StreamReader(ftpStream);
            if ((this._line = file.ReadLine()) != $"Version {assembly.GetName().Version}")
            {
                MessageBox.Show("Znaleziono update!");
                using (Process UpdaterProcess = new Process())
                {
                    UpdaterProcess.StartInfo.UseShellExecute = true;
                    UpdaterProcess.StartInfo.FileName = @".\AnonimUpdater.exe";
                    UpdaterProcess.Start();
                    System.Environment.Exit(1);
                }
            }
            else
            {
                MessageBox.Show("Posiadasz najnowszą wersję");
            }
        }
    }
}
