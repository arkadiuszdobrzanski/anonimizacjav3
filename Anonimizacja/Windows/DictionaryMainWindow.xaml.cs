﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Anonimizacja.Windows
{
    /// <summary>
    /// Logika interakcji dla klasy DictionaryMainWindow.xaml
    /// </summary>
    public partial class DictionaryMainWindow : Window
    {
        private Modules.Dictionary _dictionary;
        public string line;
        public DictionaryMainWindow()
        {
            InitializeComponent();
            this.deleteFromDictionary.IsEnabled = false;
            this._dictionary = new Modules.Dictionary(this.dictionary);
            this._dictionary.loadItemsFromFile();
        }


        private void addToDictionary_Click(object sender, RoutedEventArgs e)
        {
            this._dictionary.addItem(d_inputValue.Text);
        }

        private void deleteFromDictionary_Click(object sender, RoutedEventArgs e)
        {
            if(this.dictionary.SelectedItem == null)
            {
                this.deleteFromDictionary.IsEnabled = false;
            }
            else
            {
                this.dictionary.Items.RemoveAt(this.dictionary.Items.IndexOf(this.dictionary.SelectedItem));
                this.deleteFromDictionary.IsEnabled = false;
            }
        }

        private void saveDictionary_Click(object sender, RoutedEventArgs e)
        {
            if (this._dictionary.saveDictionary(this.dictionary))
                MessageBox.Show("Słownik zapisany");
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void dictionary_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.deleteFromDictionary.IsEnabled = true;
        }
    }
    
}
