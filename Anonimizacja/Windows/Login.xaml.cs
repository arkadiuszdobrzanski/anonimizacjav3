﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Anonimizacja.Windows
{
    /// <summary>
    /// Logika interakcji dla klasy Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        private Modules.Licences licences;
        private int i;
        public Login()
        {
            InitializeComponent();
            this.licences = new Modules.Licences();
            this.i = 3;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (licences.checkLicence(this.licenceKey.Text))
            {
                //Start start = new Start();
                //this.Close();
                //start.Show();
                this.loginDrawer.Width = 400;
                this.mainWindow.Width = 800;
                this.mainRect.Rect = new Rect(0, 0, 800, 600);
                this.mainGrid.Margin = new Thickness(0, 0, 390, 0);
                this.WindowStartupLocation = WindowStartupLocation.Manual;
                this.Left = SystemParameters.PrimaryScreenWidth - (SystemParameters.PrimaryScreenWidth/2) - (this.Width/2);

            }
            else
            {
                if(this.i < 1)
                {
                    this.Close();
                }
                else
                {
                    this.i--;
                    Layout.MessageBox messageBox = new Layout.MessageBox();
                    messageBox.message.Text = "Błędny kod. Pozostały " + i + " prób.";
                    messageBox.Show();

                }

            }
            

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            Start start = new Start();
            this.Close();
            start.Show();
        }
    }
}
