﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using Microsoft.Win32;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Anonimizacja.Windows
{
    /// <summary>
    /// Logika interakcji dla klasy MultiPDF.xaml
    /// </summary>
    public partial class MultiPDF : Window
    {
        private Modules.MultiPDF multiPDF;
        private bool IsFine = false;
        public MultiPDF()
        {
            InitializeComponent();
            this.multiPDF = new Modules.MultiPDF();
        }

        private void chooseImages_Click(object sender, RoutedEventArgs e)
        {
            this.multiPDF.setImagesPaths();
        }

        private void saveDictionary_Click(object sender, RoutedEventArgs e)
        {
            if(this.fileName.Text != null)
            {
                this.multiPDF.convertImagesToPDF(this.fileName.Text);
                this.IsFine = true;
            }
            else
            {
                MessageBox.Show("Wprowadź nazwę pliku PDF");
            }
        }

        public bool isOk()
        {
            if (IsFine)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
