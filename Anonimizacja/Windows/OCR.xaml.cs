﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Anonimizacja.Windows
{
    /// <summary>
    /// Logika interakcji dla klasy OCR.xaml
    /// </summary>
    public partial class OCR : Window
    {
        private Modules.OCR ocr;
        private string _file;
        public OCR()
        {
            InitializeComponent();
            this.ocr = new Modules.OCR();
        }

        private void addToDictionary_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image files (*.png;*.jpeg)|*.png;*.jpeg|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true)
                image_path.Content = openFileDialog.FileName;

            this._file = openFileDialog.FileName;
        }

        private void saveDictionary_Click(object sender, RoutedEventArgs e)
        {
            this.ocr.OCRtoPDF(this._file);

        }
    }
}
