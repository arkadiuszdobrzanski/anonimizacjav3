﻿#pragma checksum "..\..\..\..\Layout\Home.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "CD471010EF5A28E3943A9920F366767923883A93"
//------------------------------------------------------------------------------
// <auto-generated>
//     Ten kod został wygenerowany przez narzędzie.
//     Wersja wykonawcza:4.0.30319.42000
//
//     Zmiany w tym pliku mogą spowodować nieprawidłowe zachowanie i zostaną utracone, jeśli
//     kod zostanie ponownie wygenerowany.
// </auto-generated>
//------------------------------------------------------------------------------

using Anonimizacja;
using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Converters;
using MaterialDesignThemes.Wpf.Transitions;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using WPFSpark;


namespace Anonimizacja.Layout {
    
    
    /// <summary>
    /// Home
    /// </summary>
    public partial class Home : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 24 "..\..\..\..\Layout\Home.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grid1;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\..\..\Layout\Home.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WPFSpark.ToggleSwitch hideDic;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\..\..\Layout\Home.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WPFSpark.ToggleSwitch hideCity;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\..\..\Layout\Home.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WPFSpark.ToggleSwitch hidePesel;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\..\..\Layout\Home.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button chooseFile;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\..\..\Layout\Home.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button classic;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\..\..\Layout\Home.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button openDictionary;
        
        #line default
        #line hidden
        
        
        #line 75 "..\..\..\..\Layout\Home.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button execute;
        
        #line default
        #line hidden
        
        
        #line 79 "..\..\..\..\Layout\Home.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox searchValue;
        
        #line default
        #line hidden
        
        
        #line 80 "..\..\..\..\Layout\Home.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox tags;
        
        #line default
        #line hidden
        
        
        #line 92 "..\..\..\..\Layout\Home.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WPFSpark.ToggleSwitch isDefault;
        
        #line default
        #line hidden
        
        
        #line 96 "..\..\..\..\Layout\Home.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WPFSpark.ToggleSwitch hidePostCode;
        
        #line default
        #line hidden
        
        
        #line 107 "..\..\..\..\Layout\Home.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WPFSpark.ToggleSwitch multiPDF;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "5.0.6.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Anonimizacja;V1.0.0.0;component/layout/home.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Layout\Home.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "5.0.6.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.grid1 = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.hideDic = ((WPFSpark.ToggleSwitch)(target));
            return;
            case 3:
            this.hideCity = ((WPFSpark.ToggleSwitch)(target));
            return;
            case 4:
            this.hidePesel = ((WPFSpark.ToggleSwitch)(target));
            return;
            case 5:
            this.chooseFile = ((System.Windows.Controls.Button)(target));
            
            #line 59 "..\..\..\..\Layout\Home.xaml"
            this.chooseFile.Click += new System.Windows.RoutedEventHandler(this.chooseFile_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.classic = ((System.Windows.Controls.Button)(target));
            
            #line 61 "..\..\..\..\Layout\Home.xaml"
            this.classic.Click += new System.Windows.RoutedEventHandler(this.chooseClassic_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.openDictionary = ((System.Windows.Controls.Button)(target));
            
            #line 65 "..\..\..\..\Layout\Home.xaml"
            this.openDictionary.Click += new System.Windows.RoutedEventHandler(this.editDictionary);
            
            #line default
            #line hidden
            return;
            case 8:
            
            #line 68 "..\..\..\..\Layout\Home.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            
            #line 69 "..\..\..\..\Layout\Home.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Reset_click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.execute = ((System.Windows.Controls.Button)(target));
            
            #line 75 "..\..\..\..\Layout\Home.xaml"
            this.execute.Click += new System.Windows.RoutedEventHandler(this.execute_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            this.searchValue = ((System.Windows.Controls.TextBox)(target));
            return;
            case 12:
            this.tags = ((System.Windows.Controls.ListBox)(target));
            return;
            case 13:
            this.isDefault = ((WPFSpark.ToggleSwitch)(target));
            return;
            case 14:
            this.hidePostCode = ((WPFSpark.ToggleSwitch)(target));
            return;
            case 15:
            
            #line 106 "..\..\..\..\Layout\Home.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.settingsCLick);
            
            #line default
            #line hidden
            return;
            case 16:
            this.multiPDF = ((WPFSpark.ToggleSwitch)(target));
            return;
            case 17:
            
            #line 110 "..\..\..\..\Layout\Home.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.MultiPDF);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

